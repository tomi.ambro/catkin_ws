## This Repository is a catkin workspace

### Keep in Mind: in order to run all packages including those beyond Mediums tutorial, you will also need to run
`sudo apt-get install ros-kinetic-navigation`

## Steps (multiple terminals will be needed):

### Starting Roscore
`roscore` 

### Running Tutrlesim
`rosrun turtlesim turtlesim_node`

### Compilation
`cd thisRepo`

`catkin_make`

### Running C++ Node in one terminal
`cd thisRepo`

`source ./devel/setup.bash`

`rosrun robot_tutorial robot`

### Running Python Plugin in another terminal
`cd thisRepo`

`source ./devel/setup.bash`

`rqt --standalone rqt_mypkg --force-discover`

### Sample data points found in:
`cat thisRepo/src/rqt_mypkg/src/rqt_mypkg/points.js`

### Notes:
For some reason there's a bug I haven't been able to fix yet. The first time you upload 'points.js' the first few points get lost. And although the publisher seems to be sending them, they never arrive to the subscriber.

The issue appears to be on the plugin side, because it happens only the first time the plugin is run. And it doesn't happen when I send position points from another c++ node (dirs) that I created for debuggin purpuses.
