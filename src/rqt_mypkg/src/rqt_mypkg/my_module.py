import os
import rospy
import rospkg
import json

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget

from robot_tutorial.srv import *
from geometry_msgs.msg import Pose2D
from tkFileDialog import askopenfile

# file = '{"points": [{"x":1, "y":10, "theta":0}, {"x":10, "y":1, "theta":0}]}'

class MyPlugin(Plugin):

    def __init__(self, context):
        super(MyPlugin, self).__init__(context)
        # Give QObjects reasonable names
        self.setObjectName('MyPlugin')

        # Process standalone plugin command-line arguments
        from argparse import ArgumentParser
        parser = ArgumentParser()
        # Add argument(s) to the parser.
        parser.add_argument("-q", "--quiet", action="store_true",
                      dest="quiet",
                      help="Put plugin in silent mode")
        args, unknowns = parser.parse_known_args(context.argv())
        if not args.quiet:
            print 'arguments: ', args
            print 'unknowns: ', unknowns

        # Create QWidget
        self._widget = QWidget()
        # Get path to UI file which should be in the "resource" folder of this package
        ui_file = os.path.join(rospkg.RosPack().get_path('rqt_mypkg'), 'resource', 'MyPlugin.ui')
        # Extend the widget with all attributes and children from UI file
        loadUi(ui_file, self._widget)
        # Give QObjects reasonable names
        self._widget.setObjectName('MyPluginUi')
        # print vars(self._widget)
        add_json = self._widget.Test
        stop = self._widget.pushButton_2
        resume = self._widget.pushButton_3
        reset = self._widget.pushButton_4
        add_json.clicked.connect(self.addJsonCallback)
        stop.clicked.connect(self.stopCallback)
        resume.clicked.connect(self.resumeCallback)
        reset.clicked.connect(self.resetCallback)
        # Show _widget.windowTitle on left-top of each plugin (when 
        # it's set in _widget). This is useful when you open multiple 
        # plugins at once. Also if you open multiple instances of your 
        # plugin at once, these lines add number to make it easy to 
        # tell from pane to pane.
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))
        # Add widget to the user interface
        context.add_widget(self._widget)

    def addJsonCallback(self, msg):
        position = Pose2D()
        file = askopenfile()
        if file:
            data = json.load(file)
            dirs_pub = rospy.Publisher('directions', Pose2D, queue_size=1000)
            rate = rospy.Rate(10) # 1hz
            size = len(data["points"])
            count = 0
            while not rospy.is_shutdown():
                position.x = data["points"][count]["x"]
                position.y = data["points"][count]["y"]
                position.theta = data["points"][count]["theta"]
                print "sent: ", position.x, position.y
                dirs_pub.publish(position)
                count = count + 1
                if count >= size:
                    break
                rate.sleep()


    def stopCallback(self, msg):
        print "Stop"
        self.manual_commands_client("turtle1", "stop")

    def resumeCallback(self, msg):
        print "Resume"
        self.manual_commands_client("turtle1", "resume")

    def resetCallback(self, msg):
        print "Reset"
        self.manual_commands_client("turtle1", "reset")

    def manual_commands_client(self, name, order):
        rospy.wait_for_service('manual_commands')
        try:
            manual_commands = rospy.ServiceProxy('manual_commands', ManualCommands)
            resp1 = manual_commands(name, order)
            return
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def shutdown_plugin(self):
        # TODO unregister all publishers here
        pass

    def save_settings(self, plugin_settings, instance_settings):
        # TODO save intrinsic configuration, usually using:
        # instance_settings.set_value(k, v)
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # TODO restore intrinsic configuration, usually using:
        # v = instance_settings.value(k)
        pass

    #def trigger_configuration(self):
        # Comment in to signal that the plugin has a way to configure
        # This will enable a setting button (gear icon) in each dock widget title bar
        # Usually used to open a modal configuration dialog