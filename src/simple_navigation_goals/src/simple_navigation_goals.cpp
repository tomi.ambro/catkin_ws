#include <ros/ros.h>
#include <nav_msgs/GetMap.h>
#include <vector>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
using namespace std;

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

// Global map variables
int rows;
int cols;
double mapResolution;
double b_x, b_y;
vector<vector<bool> > grid;

void readMap(const nav_msgs::OccupancyGrid& map)
{
	ROS_INFO("Received a %d X %d map @ %.3f m/px\n", map.info.width, map.info.height, map.info.resolution);
	rows = map.info.height;
	cols = map.info.width;
	mapResolution = map.info.resolution;
	
	// Dynamically resize the grid
	grid.resize(rows);
	for (int i = 0; i < rows; i++) {
		grid[i].resize(cols);
	}
	int currCell = 0;
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
	 		if (map.data[currCell] == 0) // unoccupied cell
	 			grid[i][j] = false;
	 		else
	 			grid[i][j] = true; // occupied (100) or unknown cell (-1)
			
			currCell++;
		}
	}
}

bool requestMap(ros::NodeHandle &n)
{
	nav_msgs::GetMap::Request req;
	nav_msgs::GetMap::Response res;
	while (!ros::service::waitForService("static_map", ros::Duration(3.0))) {
		ROS_INFO("Waiting for service static_map to become available");
	}
	ROS_INFO("Requesting the map...");
	ros::ServiceClient mapClient = n.serviceClient<nav_msgs::GetMap>("static_map");
	if (mapClient.call(req, res)) {
		readMap(res.map);
		return true;
	}
	else {
		ROS_ERROR("Failed to call map service");
		return false;
	}
}

int main(int argc, char** argv) {
	float x, y, xaux, yaux;
	ros::init(argc, argv, "simple_navigation_goals");
	ros::NodeHandle n;

	if (!requestMap(n))
 		exit(-1);

	// Tell the action client that we want to spin a thread by default
	MoveBaseClient ac("move_base", true);

	// Wait for the action server to come up
	while(!ac.waitForServer(ros::Duration(5.0))){
		ROS_INFO("Waiting for the move_base action server to come up");
	}

	move_base_msgs::MoveBaseGoal goal;

	// Target goals are received in odom frame!!
	goal.target_pose.header.frame_id = "odom";
	goal.target_pose.header.stamp = ros::Time::now();
	
	cout << "x max at: " << cols * mapResolution / 2 << endl;
	cout << "y max at: " << rows * mapResolution / 2 << endl;

	cout << "Enter map X coordinate: ";
	cin >> x;
	cout << "Enter map Y coordinate: ";
	cin >> y;
	goal.target_pose.pose.orientation.w = 1.0;

	// Convert [x, y] coordinates in meter to the equivalent grid point 
	xaux = (x / mapResolution) + (cols / 2);
	yaux = (y / mapResolution) + (rows / 2);

	// Debugin
	// cout << "Grid x value: " << xaux << endl;
	// cout << "Grid y value: " << yaux << endl;
	// cout << grid[yaux][xaux] << endl;
	
	if(grid[yaux][xaux]) {
		goal.target_pose.pose.position.x = x;
		goal.target_pose.pose.position.y = y;

		ROS_INFO("Sending goal");
		ac.sendGoal(goal);
		ac.waitForResult();
		
		if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
			ROS_INFO("Target goal reached");
		else
			ROS_INFO("The base failed to to get to the goal for some reason");
		}
	else
		ROS_INFO("The coordinates specified are occupied, exiting");
	
	return 0;
}
