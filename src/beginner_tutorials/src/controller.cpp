#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>

class Controller {
	private:
		ros::NodeHandle n;
		ros::Publisher pub;
		ros::Subscriber sub;
	
	public:
		Controller() {
			sub = n.subscribe("chatter", 1000, &Controller::callback, this);
			pub = n.advertise<std_msgs::String>("feedback", 1000);
		}

		void callback(const std_msgs::String::ConstPtr &msg) {
			std_msgs::String pub_str;
			std::stringstream ss;
			ROS_INFO("Controller heard: [%s]", msg->data.c_str());			
			ss << "position set to: " << msg->data.c_str();
			pub_str.data = ss.str();

			std::cout << pub_str.data.c_str() << std::endl;

			pub.publish(pub_str);
			ros::spinOnce();
		}

};
int main(int argc, char **argv)
{
	ros::init(argc, argv, "Controller");
	Controller ctrl;
	ros::spin();

	return 0;
}