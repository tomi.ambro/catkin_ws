#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Pose2D.h"
#include "turtlesim/Pose.h"
#include <sstream>
using namespace std;

int main(int argc, char **argv)
{
	ros::init(argc, argv, "dirs");
	ros::NodeHandle n;
	ros::Publisher dirs_pub = n.advertise<geometry_msgs::Pose2D>("directions", 10);
	geometry_msgs::Pose2D goal_pose;

	ros::Rate loop_rate(0.5);

	while (ros::ok())
	{
		cout << "enter X coord: "; 
		cin >> goal_pose.x;
		cout << "enter Y coord: ";
		cin >> goal_pose.y;
		goal_pose.theta = 0;

		dirs_pub.publish(goal_pose);

		ros::spinOnce();

		loop_rate.sleep();
	}
  return 0;
}