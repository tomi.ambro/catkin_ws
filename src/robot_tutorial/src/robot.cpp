#include "ros/ros.h"
#include "robot_tutorial/ManualCommands.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Pose2D.h"
#include "turtlesim/Pose.h"
#include "turtlesim/TeleportAbsolute.h"
#include <sstream>
#include <iostream> 
#include <queue>
#include <math.h>

using namespace std;

// Global variables
const double PI = 3.14159265359;
queue <turtlesim::Pose> q;

/******* Helper functions *******/
double degrees2radians(double angle_in_degrees){
	return angle_in_degrees * PI / 180.0;
}
double getDistance(double x1, double y1, double x2, double y2) {
	return sqrt(pow((x1-x2),2)+pow((y1-y2),2));
}

// Robot class implementation
class Robot {
	private:
		ros::NodeHandle n;
		ros::Publisher vel_pub;
		ros::Publisher feedback_pub;
		ros::Subscriber pose_sub;
		ros::Subscriber messages_sub;
		ros::ServiceServer service;
		ros::ServiceClient client;
		turtlesim::TeleportAbsolute srv;
		turtlesim::Pose turtlesim_pose;
	
	public:
		string order;
		turtlesim::Pose goal_pose;
		turtlesim::Pose aux_pose;
		geometry_msgs::Pose2D feedback_msg;
		geometry_msgs::Twist vel_msg;

		Robot() {
			// Advertise to turtlesim
			vel_pub = n.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel", 10);
			// Subscribe to new directions
			messages_sub = n.subscribe("directions", 10, &Robot::dirCallback, this, ros::TransportHints().tcpNoDelay());
			// Get turtlesim position
			pose_sub = n.subscribe("/turtle1/pose", 10, &Robot::poseCallback, this);
			// Send feedback position
			feedback_pub = n.advertise<geometry_msgs::Pose2D>("feedback", 10);
			// Create the manual commands service
			service = n.advertiseService("manual_commands", &Robot::process, this);
			// Connect to turtlesim's client to reset
			client = n.serviceClient<turtlesim::TeleportAbsolute>("/turtle1/teleport_absolute");
			order = " ";
		}

		void toGoal(turtlesim::Pose goal_pose, double tolerance) {
			const double Kv = 1.5;
			const double Kh = 3.5;

			ros::Rate loop_rate(10);

			 while(getDistance(turtlesim_pose.x, turtlesim_pose.y, goal_pose.x, goal_pose.y) > tolerance) {
				if(order == "stop" || order == "reset") {
					vel_msg.linear.x = 0;
					vel_msg.angular.z = 0;
					if(order == "reset") {
						goal_pose.x = 5.54;
						goal_pose.y = 5.54;
						goal_pose.theta = 0;
						order = " ";
					}
				}
				else {
					vel_msg.linear.x = Kv * getDistance(turtlesim_pose.x, turtlesim_pose.y, goal_pose.x, goal_pose.y);
					vel_msg.linear.y = 0;
					vel_msg.linear.z = 0;
					vel_msg.angular.x = 0;
					vel_msg.angular.y = 0;
					vel_msg.angular.z = Kh * (atan2(goal_pose.y - turtlesim_pose.y, goal_pose.x - turtlesim_pose.x) - turtlesim_pose.theta);
				}

				vel_pub.publish(vel_msg);
				ros::spinOnce();
				loop_rate.sleep();
			}
			
			vel_msg.linear.x = 0;
			vel_msg.angular.z = 0;
			vel_pub.publish(vel_msg);
			ros::spinOnce();
		}

		// Directions topic callback
		void dirCallback(const geometry_msgs::Pose2D::ConstPtr &pose_message) {
			aux_pose.x = pose_message->x;
			aux_pose.y = pose_message->y;
			aux_pose.theta = pose_message->theta;
			q.push(aux_pose);
			cout << "recieved x: " << aux_pose.x << endl;
			cout << "recieved y: " << aux_pose.y << endl;
		}

		// Turtlesim current position callback
		void poseCallback(const turtlesim::Pose::ConstPtr &pose_message) {
			/* Record current position */
			turtlesim_pose.x = pose_message->x;
			turtlesim_pose.y = pose_message->y;
			turtlesim_pose.theta = pose_message->theta;

			/* Send current position as feedback */
			feedback_msg.x = pose_message->x;
			feedback_msg.y = pose_message->y;
			feedback_msg.theta = pose_message->theta;

			feedback_pub.publish(feedback_msg);
			ros::spinOnce();
		}

		// Server method
		bool process(robot_tutorial::ManualCommands::Request  &req, robot_tutorial::ManualCommands::Response &res) {
			if(req.name == "turtle1") {
				if(req.order == "stop") {
					order = req.order;
				}
				if(req.order == "resume") {
					order = "resume";
				}
				if(req.order == "reset") {
					order = "reset";
					// Takes turtlesim to starting point
					goal_pose.x = 5.54;
					goal_pose.y = 5.54;
					goal_pose.theta = 0;

					srv.request.x = goal_pose.x;
  					srv.request.y = goal_pose.y;
  					srv.request.theta = goal_pose.theta;
					client.call(srv);
				}
			}
			return true;
		}

};
int main(int argc, char **argv)
{	
	ros::init(argc, argv, "Robot");
	Robot r;

	ros::Rate loop_rate(10);
	
	while(ros::ok()) {

		if(!q.empty()) {
			r.goal_pose = q.front();
			q.pop();
		}
		r.toGoal(r.goal_pose, 0.01);
		loop_rate.sleep();
	}
	ros::spin();
	return 0;
}